FROM ubuntu:20.04

ARG ABRANCH_TAG

ARG AREPO_URL
ARG ABLOCKCHAIN_PATH_NAME
ARG APROGRAM_NAME
ARG ACONFIG_PATH
ARG ACONFIG_NET

ENV TZ="UTC"
ENV BLOCKCHAIN_PATH_NAME=$ABLOCKCHAIN_PATH_NAME
ENV PROGRAM_NAME=$APROGRAM_NAME
ENV CONFIG_PATH=$ACONFIG_PATH
ENV CONFIG_NET=$ACONFIG_NET

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y bc curl lsb-release python3 tar bash ca-certificates git openssl unzip wget python3-pip sudo acl build-essential python3-dev python3.8-venv python3.8-distutils python-is-python3 vim tzdata && \
    rm -rf /var/lib/apt/lists/* && \
    ln -snf "/usr/share/zoneinfo/$TZ" /etc/localtime && echo "$TZ" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata \
&& echo "cloning ${ABRANCH_TAG}" \
&& git clone --branch ${ABRANCH_TAG} --single-branch --depth 1 ${AREPO_URL} \
&& cd ${BLOCKCHAIN_PATH_NAME} \
&& git submodule update --init mozilla-ca \
&& chmod +x install.sh \
&& /usr/bin/sh ./install.sh \
&& pip install psycopg2-binary \
       && pip install requests \
       && pip install zstd \
&& rm -rf /${BLOCKCHAIN_PATH_NAME}/.git \
&& rm -rf /${BLOCKCHAIN_PATH_NAME}/tests \
&& rm -rf /${BLOCKCHAIN_PATH_NAME}/${BLOCKCHAIN_PATH_NAME}-gui \
&& apt-get purge -y build-essential bc curl lsb-release git unzip wget acl vim \
    && apt-get autoremove -y

ENV PATH=/${BLOCKCHAIN_PATH_NAME}/venv/bin/:$PATH
WORKDIR /${BLOCKCHAIN_PATH_NAME}
ADD ./entrypoint.sh entrypoint.sh

ENTRYPOINT ["bash", "./entrypoint.sh"]
