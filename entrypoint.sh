if [[ -n "${TZ}" ]]; then
  echo "Setting timezone to ${TZ}"
  ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
fi

cd /${BLOCKCHAIN_PATH_NAME} || exit

. ./activate

${PROGRAM_NAME} init --fix-ssl-permissions
${PROGRAM_NAME} init

sed -i 's/localhost/127.0.0.1/g' ~/${CONFIG_PATH}/${CONFIG_NET}/config/config.yaml

${PROGRAM_NAME} start node

# shellcheck disable=SC2064
trap "echo 'halt command received: stopping'; ${PROGRAM_NAME} stop all -d; exit 0" SIGINT SIGTERM

while true; do sleep 1; done;